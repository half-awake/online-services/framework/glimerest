package glimerest

import (
	"context"
	"errors"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var Client *mongo.Client
var Db *mongo.Database

func Connect() error {
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	Client, err = mongo.Connect(ctx, options.Client().ApplyURI(Get("MONGODB_URL")))
	if err != nil {
		return err
	}

	err = Client.Ping(ctx, nil)

	if databaseName, exist := Lookup("DATABASE_NAME"); exist {
		Db = Client.Database(databaseName)
	} else {
		return errors.New("DATABASE_NAME env is missing")
	}

	return err
}

func Disconnect() error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	return Client.Disconnect(ctx)
}
