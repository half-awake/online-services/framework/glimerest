package glimerest

import (
	"flag"

	"github.com/gofiber/fiber"
	"github.com/gofiber/fiber/middleware"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// InitService to start the service
func InitService(serviceName string) {
	debug := flag.Bool("debug", false, "sets log level to debug")
	port := flag.Int("port", 3000, "sets the http server port")
	flag.Parse()

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		log.Warn().Msgf("%s running in debug mod!", serviceName)
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	if err := Load(); err != nil {
		log.Fatal().Msg("Error loading .env file")
	}

	if err := Connect(); err != nil {
		log.Fatal().Msgf("Error to connect to database: %v", err)
	}
	defer Disconnect()

	app := fiber.New()
	app.Use(middleware.Logger())
	setErrorHandler(app)

	log.Fatal().Msgf("Error to start server: %e", app.Listen(*port))
}

func setErrorHandler(app *fiber.App) {
	app.Settings.ErrorHandler = func(ctx *fiber.Ctx, err error) {
		code := fiber.StatusInternalServerError
		message := "Internal Server Error :("

		// Retreive the custom statuscode if it's an fiber.*Error
		if e, ok := err.(*fiber.Error); ok {
			code = e.Code
			message = e.Message
		}

		// Send custom error page
		err = ctx.Status(code).JSON(fiber.Map{"error": message})
		if err != nil {
			ctx.Status(500).SendString(message)
		}
	}
}
