module gitlab.com/glime-studio/online-services/framework/glimerest

go 1.14

require (
	github.com/gofiber/fiber v1.14.1
	github.com/gofiber/jwt v0.2.0
	github.com/joho/godotenv v1.3.0
	github.com/rs/zerolog v1.19.0
	go.mongodb.org/mongo-driver v1.4.0
)
