package glimerest

import (
	"os"

	"github.com/joho/godotenv"
)

// Load config
func Load() error {
	return godotenv.Load(".env")
}

// Get config from env value
func Get(key string) string {
	return os.Getenv(key)
}

// Lookup Get config from env value. Return false if key doesnt exist
func Lookup(key string) (string, bool) {
	return os.LookupEnv(key)
}
